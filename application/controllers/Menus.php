<?php
    class Menus extends CI_Controller
    {
      function __construct()
        {
            parent::__construct();
        }
        public function desayunos(){
          $this->load->view('header');
          $this->load->view('menus/desayunos');
          $this->load->view('footer');
        }
        public function almuerzos(){
          $this->load->view('header');
          $this->load->view('menus/almuerzos');
          $this->load->view('footer');
        }
        public function meriendas(){
          $this->load->view('header');
          $this->load->view('menus/meriendas');
          $this->load->view('footer');
        }
        public function cartas(){
          $this->load->view('header');
          $this->load->view('menus/cartas');
          $this->load->view('footer');
        }
        public function ubicacion(){
          $this->load->view('header');
          $this->load->view('menus/ubicacion');
          $this->load->view('footer');
        }
    }
?>
