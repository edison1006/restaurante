<div class="text-center">
  <h1 >RESTAURANTE UTC</h1>
  <br>
  <img src="<?php echo base_url();?>/assets/images/logorestaurante.png" alt="">
  <div class="container">
    <div class="row">
      <div class="col-md-6 text-center">
        <h2>Mision<h2>
        <p class="text-justify">Ofrecer una experiencia gastronómica completa y satisfactoria que cumpla con las expectativas de sus clientes y los haga sentir bienvenidos y satisfechos.</p>
        <br>
      </div>
      <div class="col-md-6 text-center">
        <h2>Vision<h2>
          <p class="text-justify">Ser reconocido como el mejor restaurante de la ciudad en términos de calidad de comida, servicio y ambiente, además de convertirse en un destino gastronómico de renombre a nivel nacional o internacional, a través de la oferta de platos únicos y una experiencia gastronómica inolvidable.</p>
      </div>
    </div>
  </div>
<div>
