<div class="text-center">
  <h1>ALMUERZOS</h1>
  <br>
  <table class="table table-bordered heigth:">
    <tr>
      <th class="text-center">Nombre:</th>
      <th class="text-center">Descripción:</th>
      <th class="text-center">Precio:</th>
      <th class="text-center">Foto:</th>
    </tr>
    <tr>
      <td class="text-center"> Sopa y Arroz con pollo frito</td>
      <td class="text-center"> Sopa + Arroz + Menestra + Pollo frito + Jugo</td>
      <td class="text-center"> $2.50</td>
      <td class="text-center"> <img src="<?php echo base_url();?>/assets/images/pollofrito.jpg" alt="" style="width:100%; height:100px;" ></td>
    </tr>
    <tr>
      <td class="text-center"> Seco de Pollo</td>
      <td class="text-center"> Sopa + Arroz Amarillo + Jugo de pollo + Jugo</td>
      <td class="text-center"> $2.50</td>
      <td class="text-center"> <img src="<?php echo base_url();?>/assets/images/seco.jpg" alt="" style="width:100%; height:100px;" ></td>
    </tr>
    <tr>
      <td class="text-center"> Chuleta Asada</td>
      <td class="text-center"> Sopa + Arroz + Menestra + Chuleta Asada + Jugo</td>
      <td class="text-center"> $3 .00</td>
      <td class="text-center"> <img src="<?php echo base_url();?>/assets/images/completo.jpg" alt="" style="width:100%; height:100px;" ></td>
    </tr>
  </table>
</div>
