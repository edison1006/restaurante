<div class="text-center">
  <h1>MERIENDAS</h1>
  <br>
  <table class="table table-bordered heigth:">
    <tr>
      <th class="text-center">Nombre:</th>
      <th class="text-center">Descripción:</th>
      <th class="text-center">Precio:</th>
      <th class="text-center">Foto:</th>
    </tr>
    <tr>
      <td class="text-center"> Humitas con cafe</td>
      <td class="text-center"> Humitas + Taza de café</td>
      <td class="text-center"> $1.50</td>
      <td class="text-center"> <img src="<?php echo base_url();?>/assets/images/hummitas.jpg" alt="" style="width:100%; height:100px;" ></td>
    </tr>
    <tr>
      <td class="text-center"> Empanadas de viento</td>
      <td class="text-center"> Empanadas (queso, carne o pollo) + Taza de café</td>
      <td class="text-center"> $2.00</td>
      <td class="text-center"> <img src="<?php echo base_url();?>/assets/images/empanadas.jpg" alt="" style="width:100%; height:100px;" ></td>
    </tr>
    <tr>
      <td class="text-center"> Merienda completa</td>
      <td class="text-center"> Jugo + Seco + Taza de café + Pan</td>
      <td class="text-center"> $3.25</td>
      <td class="text-center"> <img src="<?php echo base_url();?>/assets/images/meriendacom.jpg" alt="" style="width:100%; height:100px;" ></td>
    </tr>
  </table>
</div>
