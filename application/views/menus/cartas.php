<div class="text-center">
  <h1>PLATOS A LA CARTA</h1>
  <br>
  <table class="table table-bordered heigth:">
    <tr>
      <th class="text-center">Nombre:</th>
      <th class="text-center">Descripción:</th>
      <th class="text-center">Precio:</th>
      <th class="text-center">Foto:</th>
    </tr>
    <tr>
      <td class="text-center"> Sopa Marinera</td>
      <td class="text-center"> La sopa marinera es una receta típica y tradicional de la gastronomía ecuatoriana hecha a base de mariscos</td>
      <td class="text-center"> $10.00</td>
      <td class="text-center"> <img src="<?php echo base_url();?>/assets/images/sopamar.jpg" alt="" style="width:100%; height:100px;" ></td>
    </tr>
    <tr>
      <td class="text-center"> Fritada</td>
      <td class="text-center"> Mote + Tortillas + Fritada + Ensalada</td>
      <td class="text-center"> $4.50</td>
      <td class="text-center"> <img src="<?php echo base_url();?>/assets/images/firtada.jpg" alt="" style="width:100%; height:100px;" ></td>
    </tr>
    <tr>
      <td class="text-center"> Pargo Frito</td>
      <td class="text-center"> Pescado Pargo + Patacones + Ensalada</td>
      <td class="text-center"> $5.25</td>
      <td class="text-center"> <img src="<?php echo base_url();?>/assets/images/pargo.png" alt="" style="width:100%; height:100px;" ></td>
    </tr>
  </table>
</div>
