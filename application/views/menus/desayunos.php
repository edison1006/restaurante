<div class="text-center">
  <h1>DESAYUNOS</h1>
  <br>
  <table class="table table-bordered heigth:">
    <tr>
      <th class="text-center">Nombre:</th>
      <th class="text-center">Descripción:</th>
      <th class="text-center">Precio:</th>
      <th class="text-center">Foto:</th>
    </tr>
    <tr>
      <td class="text-center"> Bolon de verde</td>
      <td class="text-center"> Bolon de verde + Taza de café + Huevo + Jugo</td>
      <td class="text-center"> $1.00</td>
      <td class="text-center"> <img src="<?php echo base_url();?>/assets/images/bolon.jpg" alt="" style="width:100%; height:100px;" ></td>
    </tr>
    <tr>
      <td class="text-center"> Desayuno continental</td>
      <td class="text-center"> Jugo + Taza de café + Huevo + Pan</td>
      <td class="text-center"> $2.50</td>
      <td class="text-center"> <img src="<?php echo base_url();?>/assets/images/desayunocontinental.jpg" alt="" style="width:100%; height:100px;" ></td>
    </tr>
    <tr>
      <td class="text-center"> Desayuno completo</td>
      <td class="text-center"> Jugo + Arroz + Taza de café + Huevo + Pan</td>
      <td class="text-center"> $3.25</td>
      <td class="text-center"> <img src="<?php echo base_url();?>/assets/images/completo.jpg" alt="" style="width:100%; height:100px;" ></td>
    </tr>
  </table>
</div>
